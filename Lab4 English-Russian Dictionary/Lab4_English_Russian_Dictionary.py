import sys
class Map:
    
  data=[]

  def add(self, word, translation):
      pair = tuple()
      pair = (word, translation)
      self.data.append(pair)

  def search(self, word):
      flag = False
      for key in self.data:
        if word == key[0]:
          flag = True
          print(key)
      return flag
        

  def remove(self, word):
      newdata = []
      for key in self.data:
        if word != key[0]:
          pair = tuple()
          pair = (key[0], key[1])
          newdata.append(pair)
      self.data = newdata

  def Print(self):
      for key in self.data:
        print(key)


dict = Map()

flag = True
while flag:
  data = input().split()
  if data[0]=="add":
    dict.add(data[1], data[2]) 
  elif data[0]=="search":
    if not dict.search(data[1]):
      print("Не найден")
  elif data[0]=="remove":
    dict.remove(data[1]) 
  elif data[0]=="exit":
    flag = False 
  elif data[0]=="print":
    dict.Print()
